Qu'est-ce que le Clozaril ?
Clozaril (clozapine) est un médicament antipsychotique. Il agit en modifiant l'action de substances chimiques dans le cerveau.

[https://www.pharmacie-vezere.com/promotions/clozaril/](https://www.pharmacie-vezere.com/promotions/clozaril/)

Le Clozaril est utilisé pour traiter la schizophrénie après l'échec des autres traitements.


Le Clozaril est disponible uniquement auprès d'une pharmacie certifiée dans le cadre d'un programme spécial.

Le Clozaril est également utilisé pour réduire le risque de comportement suicidaire chez les personnes atteintes de schizophrénie ou de troubles similaires.

Mises en garde
Le Clozaril affecte votre système immunitaire. Vous pouvez contracter des infections plus facilement, même des infections graves ou mortelles. Appelez votre médecin si vous avez de la fièvre, un mal de gorge, une faiblesse ou un manque d'énergie.

Le Clozaril peut augmenter votre risque de convulsions, surtout à fortes doses. Évitez toute activité qui pourrait être dangereuse si vous avez une crise ou si vous perdez conscience.

Le Clozaril peut causer de graves problèmes cardiaques. Appelez immédiatement votre médecin si vous avez des douleurs à la poitrine, des difficultés à respirer, des battements de cœur rapides ou violents ou des étourdissements soudains.

Le Clozaril n'est pas approuvé pour les personnes âgées souffrant de psychose liée à la démence.

Avant de prendre ce médicament
Le Clozaril peut augmenter le risque de décès chez les personnes âgées atteintes de psychose liée à la démence et n'est pas approuvé pour cette utilisation.

Vous ne devez pas prendre Clozaril si vous êtes allergique à la clozapine.

Pour vous assurer que le Clozaril est sans danger pour vous, dites à votre médecin si vous avez déjà eu :

des problèmes cardiaques, de l'hypertension artérielle,

une crise cardiaque ou un accident vasculaire cérébral (y compris un "mini-accident vasculaire cérébral") ;

un syndrome du QT long (chez vous ou un membre de votre famille) ;

un déséquilibre électrolytique (tel qu'un faible taux de potassium ou de magnésium dans votre sang) ;

une crise d'épilepsie, une blessure à la tête ou une tumeur au cerveau ;

le diabète, ou des facteurs de risque tels que l'excès de poids ou des antécédents familiaux de diabète ;

un taux élevé de cholestérol ou de triglycérides ;

constipation ou problèmes de transit intestinal ;

une maladie du foie ou des reins ;

une hypertrophie de la prostate ou des problèmes de miction ;

glaucome ;

malnutrition ou déshydratation ; ou

si vous fumez.

La prise d'un médicament antipsychotique au cours des 3 derniers mois de la grossesse peut entraîner des problèmes respiratoires, des problèmes d'alimentation ou des symptômes de sevrage chez le nouveau-né. Cependant, vous pouvez avoir des symptômes de sevrage ou d'autres problèmes si vous arrêtez de prendre votre médicament pendant la grossesse. Si vous tombez enceinte, prévenez immédiatement votre médecin. N'arrêtez pas de prendre le Clozaril sans l'avis de votre médecin.


Vous ne devez pas allaiter pendant que vous utilisez le Clozaril.

L'utilisation de Clozaril n'est pas approuvée pour les personnes âgées de moins de 18 ans.

Comment dois-je prendre le Clozaril ?
Prenez le Clozaril exactement comme prescrit par votre médecin. Suivez toutes les instructions figurant sur l'étiquette de votre ordonnance et lisez tous les guides de médicaments ou les feuilles d'instructions. Votre médecin peut occasionnellement modifier votre dose.

Vous pouvez prendre le Clozaril avec ou sans nourriture.

La Clozapine affecte votre système immunitaire et peut avoir des effets durables sur votre corps. Vous pouvez contracter des infections plus facilement, même des infections graves ou mortelles. Il se peut que vous ayez à subir des examens médicaux fréquents pendant que vous utilisez ce médicament et pendant une courte période après votre dernière dose.

Vous ne devez pas arrêter brusquement de prendre la clozapine. Suivez les instructions de votre médecin concernant la diminution de votre dose.

Si vous recommencez à prendre du Clozaril après 2 jours ou plus d'arrêt du médicament, vous devrez peut-être utiliser une dose plus faible que celle utilisée lors de l'arrêt. Suivez très attentivement les instructions de votre médecin concernant la posologie.

Votre médecin peut vous recommander d'utiliser un laxatif pendant que vous prenez du Clozaril. N'utilisez que le type de laxatif recommandé par votre médecin.

Conservez ce médicament à température ambiante, à l'abri de l'humidité et de la chaleur.

Information sur la posologie
Dose habituelle pour les adultes atteints de schizophrénie :

Dose initiale : 12,5 mg par voie orale une ou deux fois par jour.
Titration et dose d'entretien : Peut augmenter la dose quotidienne totale par paliers de 25 mg à 50 mg par jour jusqu'à une dose cible de 300 mg à 450 mg par jour (administrée en doses fractionnées) à la fin de la deuxième semaine. Les augmentations ultérieures de la dose peuvent se faire par incréments allant jusqu'à 100 mg une ou deux fois par semaine.
Dose maximale : 900 mg par jour

Commentaires :
-La numération absolue des neutrophiles (NAN) doit être supérieure ou égale à 1500/microL pour la population générale et d'au moins 1000/microL pour les patients présentant une neutropénie ethnique bénigne (NEB) documentée avant l'instauration du traitement ; pour poursuivre le traitement, la NAN doit être contrôlée régulièrement.
-Une faible dose initiale, une titration graduelle et des doses divisées sont nécessaires pour minimiser le risque d'hypotension orthostatique, de bradycardie et de syncope.
-Lorsque le traitement est interrompu pendant 2 jours ou plus, il faut recommencer avec 12,5 mg une ou deux fois par jour ; en fonction de la tolérance, la dose recommencée peut être augmentée jusqu'à la dose thérapeutique antérieure plus rapidement que pour le traitement initial.

Utilisations :
-Pour le traitement des patients sévèrement atteints de schizophrénie qui ne répondent pas de manière adéquate au traitement antipsychotique standard.
-Pour réduire le risque de comportement suicidaire récurrent chez les patients atteints de schizophrénie ou de troubles schizo-affectifs, qui sont jugés à risque chronique de revivre un comportement suicidaire, sur la base de leurs antécédents et de leur état clinique récent.
